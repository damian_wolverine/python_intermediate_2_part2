class Player:
    def __init__(self, name: str, age: int, monthly_salary: int, team=None):
        self.name = name
        self.age = age
        self.monthly_salary = monthly_salary
        self.team = team

    @property
    def introduce(self):
        if self.team: # zwyczajnie możemy
            return f'I am {self.name} I play for {self.team}'
            # continue do pętli for!!
        return f"I am {self.name} I don't have a team"

    def __str__(self):
        return self.introduce