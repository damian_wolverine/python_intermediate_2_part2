"""
Abstrakcyjna klasa Figure z metodami: circumference oraz area

Klasy dziedziczące: Square, Rectangle, Triangle, Circle z
                    odpowiednimy atrybutami i z implementacją
                    metod circuit oraz area

Za pomocą mechanizmu wyjątków uniemożliwij utworzenie
figur z zerową bądź ujemną wartością atrybutów


- Napisz funkcję, która przyjmuję listę Figur
  a zwraca długość boku największego kwadratu znajdującego
  się na tej liście

  def biggest_square_side(figures: List[Figure])


- Napisz funkcję average_areas, która przyjmuje listę figur
  oraz opcjonalny argument figure_type. Jeżeli figure_type będzie
  równe np Triangle to zwróć średnie pole wszystkich trójkątów
  znajdujących się na liście. W przeciwnym wypadku średnią pól
  wszystkich figur

- z Gwiazdką
  Napisz funkcję która przyjmie prostokąt a zwróci listę boków kwadratów
  jakie można utworzyć z podziału takiego prostokąta. Jak na tablicy
"""

import abc


class Figure(abc.ABC):
    @abc.abstractmethod
    def area(self):
        pass

    @abc.abstractmethod
    def circumference(self):
        pass


class Rectangle(Figure):
    def __init__(self, side1: int, side2: int):
        self.side1 = side1
        self.side2 = side2

    def area(self):
        return self.side1 * self.side2

    def circumference(self):
        return self.side1 * 2 + self.side2 * 2


class Square(Rectangle):
    def __init__(self, side1: int):
        super().__init__(side1, side1)


class Triangle(Figure):
    pass


class Circle(Figure):
    pass

# [3,2,1,1]
if __name__ == '__main__':
    square1 = Square(5)
    print(square1.area())
    print(square1.circumference())

