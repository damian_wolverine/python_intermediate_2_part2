from typing import List, Dict, Tuple

"""
Zdefiniuj nowy typ danych Businessman, który dziedziczy po
Person. Dodaj do niego nowy atrybut: tax_id jako napis
Zdefiniuj nową klasę BusinessRegister, która będzie miała 
jeden atrybut: listę zarejestrowanych przedsiębiorstw. (businesses)
    - Dodaj funkcję add_business przyjmująca jeden argument będący instancją klasy
      Businessman, która doda go do rejestru tylko wtedy gdy jego tax_id jest długości 10
      W przeciwnym wypadku rzuca wyjątek, który musisz zaimplementować (pamiętajmy,
      żeby dziedziczył po exception)
"""


class Person:
    def __init__(self, name: str, age: int):
        self.name = name
        self.age = age
        self.backpack: List[str] = []

    def input_to_backpack(self, thing: str) -> None:
        self.backpack.append(thing)


class Businessman(Person):
    def __init__(self, name: str, age: int, tax_id: str):
        super().__init__(name, age)
        self.tax_id = tax_id


class TaxIdIsNotEqual10(Exception):
    pass


class BusinessRegister:
    def __init__(self):
        self.list_of_registry_businessmans = []

    def add_business(self, businesman: Businessman):
        if len(businesman.tax_id) == 10:
            self.list_of_registry_businessmans.append(businesman)
            return f"{businesman.name} has correct tax_id"
            # zwracam zwykłego stringo i musze go zapisac do zmiennej w main i wydrukować
        else:
            raise TaxIdIsNotEqual10(f'Tax id is notequal 10: {businesman.tax_id}')


## moje coś
def sum_numbers_from_file(path):
    try:
        file = open(path)
        result = 0

        for line in file:
            line = int(line.strip())
            result += line


    except FileNotFoundError as e:
        print(e)
    finally:
        print('CLosing file')
        file.close()


## od Piotrka
def sum_numbers_from_file(path):
    try:
        file = open(path)
        result = 0

        try:
            for line in file:
                line = int(line.strip())
                result += line
        except ValueError as e:
            print(e)
        finally:
            print('Closing file')
            file.close()

    except FileNotFoundError as e:
        print(e)


if __name__ == '__main__':
    businessman1 = Businessman('Piotr', 23, '1234')
    businessman2 = Businessman('Damian', 29, '1234567891')

    registry = BusinessRegister()

    try:
        registry.add_business(businessman1)
    except TaxIdIsNotEqual10 as e:
        print(e)
    try:
        registry.add_business(businessman2)
    except TaxIdIsNotEqual10 as e:
        print(e)

    # sum_numbers_from_file('words.txt')
