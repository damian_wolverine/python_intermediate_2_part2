from players import Player
from typing import List, Dict, Tuple


class Team:
    def __init__(self, name: str, abbreviation: str):
        self.name = name
        self.abbreviation = abbreviation
        self.players: List[Player] = []  # self.players = []

    def hire_player(self, player: Player):
        self.players.append(player)
        player.team = self

    def fire_player(self, player_name: str):
        list_of_players_name = [player.name for player in self.players]
        if player_name in list_of_players_name:
            selected_player = list_of_players_name.index(player_name)
            player = self.players.pop(selected_player)
            player.team = None
        else:
            pass

    def __str__(self):
        return f"{self.name} ({self.abbreviation})"


if __name__ == '__main__':
    team1 = Team("Arsenal Londyn", "AL")
    print(team1.name)
    print(team1.players)
    player1 = Player("Pierre-Emerick Aubameyang", 30, 50000, team1)
    print(player1.team)
    print(player1.name)
    team1.hire_player(player1)
    print(team1.players)
    for player in team1.players:
        print(player)
    print(len(team1.players))
    print(player1)
    # team1.fire_player("Pierre-Emerick Aubameyang")
    # print(len(team1.players))
    # print(player1.team)
