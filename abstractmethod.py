from typing import List
import abc


class MusicalInstrument(abc.ABC):
    def __init__(self, weight: float):
        self.weight = weight

    @abc.abstractmethod
    def play(self):
        pass


class Guitar(MusicalInstrument):
    def __init__(self, weight: float):
        super().__init__(weight)
    # kolejny wydruk bez poniższych

    def play(self):
        return "Guitar plays"

class Flute(MusicalInstrument):
    def __init__(self, weight: float):
        super().__init__(weight)

    def play(self):
        return "Flute plays"

class Violin(MusicalInstrument):
    def __init__(self, weight: float, bow_length: int):
        super().__init__(weight)
        self.bow_length = bow_length

    def play(self):
        return "Violin plays"

def play_instruments(instruments: List[MusicalInstrument]):
    for instrument in instruments:
        print(instrument.play())
        # return instrument.play() to tylko zwroci nam jeden obrót pętli


if __name__ == '__main__':
    # instrument = MusicalInstrument(1.1)
    # instrument = MusicalInstrument(1.1)
    # TypeError: Can't instantiate abstract class MusicalInstrument with abstract methods play
    guitar = Guitar(1.1)
    # print(guitar.play())
    flute = Flute(2.2)
    # print(flute.play())
    violin = Violin(5.0, 9)
    # print(violin.play())
    print(play_instruments([guitar, flute, violin])) # nie dajemy nreturn i dostajemy pusty zwrot z funkcji
