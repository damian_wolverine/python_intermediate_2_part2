### OBSŁUGIWANIE WYJĄTKÓW

def how_many_words_in_file(path):
    with open(path, 'r') as f:
        return len(list(f)) - 1


# def sum_numbers_in_file(path):
#     with open(path, 'r') as f:
#         # result = sum([int(number.strip()) for number in f])
#         result = 0
#
#         try:
#             for number in f:
#                 result += int(number.strip())
#
#         except ValueError:
#             print(f'Wrong line: {number.strip()}. End of loop')
#         return result
#
# def sum_numbers_in_file(path):
#     with open(path, 'r') as f:
#         # result = sum([int(number.strip()) for number in f])
#         result = 0
#         for number in f: # inaczej niż poprzednio bierzemy nad try except
#             try:
#                 result += int(number.strip())
#
#             except ValueError:
#                 print(f'Wrong line: {number.strip()}.')
#         return result

## Ważne aby wiedziecgdzie stawiamy for loop

def many_exceptions(path):
    with open(path, 'r') as f:
        for line in f:
            try:
                line = int(line.strip())
                print(line + 17) # dodanie lini z 17 do pliku
                print(20/line)
                f.write('ala')
            except TypeError as e:
                print(e)
            except ValueError as e:
                print(e)
            except ZeroDivisionError as e:
                print(e)
            # Exception musi byc nakoncu bo zbiera wszytskie wyjątki
            # i jesli
            except Exception as e:
                print(e)


if __name__ == '__main__':
    # try:
    #     value = how_many_words_in_file('woreds.txt')
    # except FileNotFoundError:
    #     print('Please provide correct path!')
    # print(sum_numbers_in_file('words.txt'))
    many_exceptions('words.txt')

