from typing import List, Dict, Tuple, Optional
from players import Player
from team import Team
from random import randint


class Match:
    def __init__(self, host, guest):
        self.host = host
        self.guest = guest
        self.result: Optional[Tuple[int, int]] = None

    def play(self) -> tuple:
        self.result = tuple([randint(0, 5), randint(0, 5)])
        print(self.result)

    @property
    def goals(self) -> int:
        return sum(self.result)

    def __str__(self):
        return f'{self.host} vs {self.guest}'


if __name__ == '__main__':
    t1 = Team("Manchester United", "MNU")
    t2 = Team("Arsenal Londyn", "AL")
    match1 = Match(t1, t2)
    match1.play()
    print(match1.goals)
    print(match1)
    # value = 12)
    # print(f'{value:>6}')
    # print(f'{5:>6}')
