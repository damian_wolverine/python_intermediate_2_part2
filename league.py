from typing import List, Dict, Tuple, Optional
from players import Player
from team import Team
from random import randint


class TeamStats:
    def __init__(self, points=0, wins=0, draws=0, loses=0):
        self.points = points
        self.wins = wins
        self.draws = draws
        self.loses = loses


class League:
    def __init__(self, year: int, table: Dict[Team, TeamStats], matches: List):
        self.year = year
        self.table = table
        self.matches = matches


def load_palyers(path):
    list_of_players = []
    with open(path, 'r') as f:
        for line in f:
            line = line.strip()
            line = line.split(',')
            player = Player(line[0], line[1], line[2])
            list_of_players.append(player)
    return list_of_players


def load_teams(path):
    list_of_team = []
    with open(path, 'r') as f:
        for line in f:
            line = line.strip()
            line = line.split(',')
            team = Team(line[1], line[0])
            list_of_team.append(team)
    return list_of_team


def hire_palyers_in_teams(list_of_players_from_file: List, list_of_teams_from_file: List):
    # print(id(list_of_players_from_file), id(list_of_teams_from_file))
    new_list_of_players = [player for player in list_of_players_from_file]
    # print(len(new_list_of_players), new_list_of_players[0])
    num_of_iter = 10
    while num_of_iter > 0:
        print(num_of_iter)
        for team in list_of_teams_from_file:
            print(team)
            index_of_player = randint(0, len(new_list_of_players) - 1)
            print(index_of_player)
            print(new_list_of_players[index_of_player].name)
            team.hire_player(new_list_of_players[index_of_player])
            del new_list_of_players[index_of_player]
        num_of_iter -= 1


def generate_matches():
    pass


if __name__ == '__main__':
    file_players = load_palyers('players.txt')
    print(id(file_players))
    print(file_players[5])
    file_teams = load_teams('teams.txt')
    print(id(file_teams))
    print(file_teams[0])
    print(len(file_teams))
    print(len(file_players))
    hire_palyers_in_teams(file_players, file_teams)
    for team in file_teams:
        # names_of_players_in_teams = [player_in_team.name for player_in_team in team.players]
        print(f"Club name's: {team.name}, list of players {len(team.players)}")
        # print(f"CLub name's: {team.name}, list of players {team.players}")
    print(file_players[5].name)
