from typing import List, Dict, Tuple


class Person:
    def __init__(self, name: str, age: int):
        self.name = name
        self.age = age
        self.backpack: List[str] = []

    def input_to_backpack(self, thing: str) -> None:
        self.backpack.append(thing)


class SellingAlcoholToUnderAgePersonError(Exception):
    pass


def sell_beer(person: Person):
    if person.age >= 18:
        person.input_to_backpack('American Pale Ale')
        print('Sold!')
    else:
        raise SellingAlcoholToUnderAgePersonError('Trying to sell person under ')


if __name__ == '__main__':
    person1 = Person('Piotr', 24)
    person2 = Person('Damian', 50)
    person3 = Person('Tomasz', 17)

    try:
        sell_beer(person1)
    except SellingAlcoholToUnderAgePersonError as e:
        print(e)
    try:
        sell_beer(person3)
    except SellingAlcoholToUnderAgePersonError as e:
        print(e)